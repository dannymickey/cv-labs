import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
img = cv.imread('source.jpg',0)
myImg =  cv.imread('otsu.jpg',0)
ret,thresh1 = cv.threshold(img, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
titles = ['MyFunction','OPENCV FUNCTION']
images = [myImg, thresh1]
for i in range(2):
    plt.subplot(1,2, i+1),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()
