import numpy as np
from numpy import median
import cv2
#
# print(cv2.getBuildInformation())
# exit(1)

def med(img, rank):
    height, width = img.shape[:2]
    vis = np.zeros((height, width, 3), np.uint8)
    av = round(rank * rank / 2)
    print(av)
    for i in range(1, height):
        for j in range(1, width):
            red = []
            green = []
            blue = []
            for ki in range (i, i + rank):
                for kj in range (j, j + rank):
                    if ki >= height and kj >= width:
                        red.append(img[ki-height, kj-width, 0])
                        green.append(img[ki-height, kj-width, 1])
                        blue.append(img[ki-height, kj-width, 2])
                    elif ki >= height:
                        red.append(img[ki-height, kj, 0])
                        green.append(img[ki-height, kj, 1])
                        blue.append(img[ki-height, kj, 2])
                    elif kj >= width:
                        red.append(img[ki, kj-width, 0])
                        green.append(img[ki, kj-width, 1])
                        blue.append(img[ki, kj-width, 2])
                    else:
                        red.append(img[ki, kj, 0])
                        green.append(img[ki, kj, 1])
                        blue.append(img[ki, kj, 2])
            red_val = median(red)
            green_val = median(green)
            blue_val = median(blue)

            # for ki in range (i, i + rank):
            #     for kj in range (j, j + rank):
            vis[i, j] = [red_val, green_val, blue_val]
    return vis

def average(img, rank):
    height, width = img.shape[:2]
    vis = np.zeros((height, width, 3), np.uint8)

    for i in range(1, height):
        for j in range(1, width):
            red = 0
            green = 0
            blue = 0
            for ki in range (i, i + rank):
                for kj in range (j, j + rank):
                    if ki >= height and kj >= width:
                        red = red + img[ki - height, kj - width, 0]
                        green = green + img[ki - height, kj - width, 1]
                        blue = blue + img[ki - height, kj - width, 2]
                    elif ki >= height:
                        red = red + img[ki - height, kj, 0]
                        green = green + img[ki - height, kj, 1]
                        blue = blue + img[ki - height, kj, 2]
                    elif kj >= width:
                        red = red + img[ki - height, kj - width, 0]
                        green = green + img[ki - height, kj - width, 1]
                        blue = blue + img[ki - height, kj - width, 2]
                    else:
                        red = red + img[ki, kj, 0]
                        green = green + img[ki, kj, 1]
                        blue = blue + img[ki, kj, 2]

            red = red / rank / rank
            green = green / rank / rank
            blue = blue / rank / rank
            for ki in range (i, i + rank):
                for kj in range (j, j + rank):
                    if ki < height and kj < width:
                        vis[i, j] = [red, green, blue]

    return vis

def sobel(img, rank = 3):
    height, width = img.shape[:2]
    vis = np.zeros((height, width, 3), np.uint8)
    vis[:height, :width,:3] = img[:height, :width, :3]
    max_r = 0
    max_g = 0
    max_b = 0

    for x in range(2, height-rank):
        for y in range(2, width-rank):
            z1_r = img[x,y][0] / 3
            z1_g = img[x,y][1] / 3
            z1_b = img[x,y][2] / 3

            z2_r = img[x+1,y][0] / 3
            z2_g = img[x+1,y][1] / 3
            z2_b = img[x+1,y][2] / 3

            z3_r = img[x+2,y][0] / 3
            z3_g = img[x+2,y][1] / 3
            z3_b= img[x+2,y][2] / 3

            z4_r = img[x,y+1][0] / 3
            z4_g = img[x,y+1][1] / 3
            z4_b = img[x,y+1][2] / 3

            z5_r = img[x+1,y+1][0] / 3
            z5_g = img[x+1,y+1][1] / 3
            z5_b = img[x+1,y+1][2] / 3

            z6_r = img[x+2,y+1][0] / 3
            z6_g = img[x+2,y+1][1] / 3
            z6_b = img[x+2,y+1][2] / 3

            z7_r = img[x,y+2][0] / 3
            z7_g = img[x,y+2][1] / 3
            z7_b = img[x,y+2][2] / 3

            z8_r = img[x+1,y+2][0] / 3
            z8_g = img[x+1,y+2][1] / 3
            z8_b = img[x+1,y+2][2] / 3

            z9_r = img[x+2,y+2][0] / 3
            z9_g = img[x+2,y+2][1] / 3
            z9_b = img[x+2,y+2][2] / 3

            Gx = (z7_r + 2 * z8_r + z9_r) - (z1_r + 2 * z2_r + z3_r)
            Gy = (z3_r + 2 * z6_r + z9_r) - (z1_r + 2 * z4_r + z7_r)
            length_r = np.sqrt((Gx * Gx) + (Gy * Gy))
            if length_r > max_r:
                max_r = length_r
            vis[x+1, y+1][0] = length_r

            Gx = (z7_g + 2 * z8_g + z9_g) - (z1_g + 2 * z2_g + z3_g)
            Gy = (z3_g + 2 * z6_g + z9_g) - (z1_g + 2 * z4_g + z7_g)
            length_g = np.sqrt((Gx * Gx) + (Gy * Gy))
            if length_g > max_g:
                max_g = length_g
            vis[x+1, y+1][1] = length_g

            Gx = (z7_b + 2 * z8_b + z9_b) - (z1_b + 2 * z2_b + z3_b)
            Gy = (z3_b + 2 * z6_b + z9_b) - (z1_b + 2 * z4_b + z7_b)
            length_b = np.sqrt((Gx * Gx) + (Gy * Gy))
            if length_b > max_b:
                max_b = length_b
            vis[x+1, y+1][2] = length_b


    for x in range(2, height-rank):
        for y in range(2, width-rank):
            vis[x+1, y+1][0] = vis[x+1, y+1][0] / max_r * 255
            vis[x+1, y+1][1] = vis[x+1, y+1][1] / max_g * 255
            vis[x+1, y+1][2] = vis[x+1, y+1][2] / max_b * 255
    for x in range(2, height-rank):
        for y in range(2, width-rank):
            if vis[x, y][0] > 50 or vis[x, y][1] > 50 or vis[x, y][2] > 50:
                vis[x, y][0] = 255
                vis[x, y][1] = 255
                vis[x, y][2] = 255
            else:
                vis[x, y][0] = 0
                vis[x, y][1] = 0
                vis[x, y][2] = 255

    return vis

def prewitt(img, rank = 3):
    height, width = img.shape[:2]
    vis = np.zeros((height, width, 3), np.uint8)
    vis[:height, :width,:3] = img[:height, :width, :3]
    max_r = 0
    max_g = 0
    max_b = 0
    for x in range(1, height-rank):
        print(height-x)
        for y in range(1, width-rank):

            z1_r = img[x,y][0] / 3
            z1_g = img[x,y][1] / 3
            z1_b = img[x,y][2] / 3

            z2_r = img[x+1,y][0] / 3
            z2_g = img[x+1,y][1] / 3
            z2_b = img[x+1,y][2] / 3

            z3_r = img[x+2,y][0] / 3
            z3_g = img[x+2,y][1] / 3
            z3_b= img[x+2,y][2] / 3

            z4_r = img[x,y+1][0] / 3
            z4_g = img[x,y+1][1] / 3
            z4_b = img[x,y+1][2] / 3

            z5_r = img[x+1,y+1][0] / 3
            z5_g = img[x+1,y+1][1] / 3
            z5_b = img[x+1,y+1][2] / 3

            z6_r = img[x+2,y+1][0] / 3
            z6_g = img[x+2,y+1][1] / 3
            z6_b = img[x+2,y+1][2] / 3

            z7_r = img[x,y+2][0] / 3
            z7_g = img[x,y+2][1] / 3
            z7_b = img[x,y+2][2] / 3

            z8_r = img[x+1,y+2][0] / 3
            z8_g = img[x+1,y+2][1] / 3
            z8_b = img[x+1,y+2][2] / 3

            z9_r = img[x+2,y+2][0] / 3
            z9_g = img[x+2,y+2][1] / 3
            z9_b = img[x+2,y+2][2] / 3

            Gx = (z7_r + z8_r + z9_r) - (z1_r + z2_r + z3_r)
            Gy = (z3_r + z6_r + z9_r) - (z1_r + z4_r + z7_r)
            length_r = np.sqrt((Gx * Gx) + (Gy * Gy))
            if length_r > max_r:
                max_r = length_r
            vis[x+1, y+1][0] = length_r

            Gx = (z7_g + z8_g + z9_g) - (z1_g + z2_g + z3_g)
            Gy = (z3_g + z6_g + z9_g) - (z1_g + z4_g + z7_g)
            length_g = np.sqrt((Gx * Gx) + (Gy * Gy))
            if length_g > max_g:
                max_g = length_g
            vis[x+1, y+1][1] = length_g

            Gx = (z7_b + z8_b + z9_b) - (z1_b + z2_b + z3_b)
            Gy = (z3_b + z6_b + z9_b) - (z1_b + z4_b + z7_b)
            length_b = np.sqrt((Gx * Gx) + (Gy * Gy))
            if length_b > max_b:
                max_b = length_b
            vis[x+1, y+1][2] = length_b


    for x in range(1, height-rank):
        for y in range(1, width-rank):
            vis[x+1, y+1][0] = vis[x+1, y+1][0] / max_r * 255
            vis[x+1, y+1][1] = vis[x+1, y+1][1] / max_g * 255
            vis[x+1, y+1][2] = vis[x+1, y+1][2] / max_b * 255
    return vis

def laplas(img, rank = 3):
    height, width = img.shape[:2]
    vis = np.zeros((height, width, 3), np.uint8)
    vis[:height, :width,:3] = img[:height, :width, :3]
    max_r = 0
    max_g = 0
    max_b = 0
    for x in range(1, height-rank):
        print(height-x)
        for y in range(1, width-rank):
            z2_r = img[x+1,y][0] / 1.0
            z2_g = img[x+1,y][1] / 1.0
            z2_b = img[x+1,y][2] / 1.0

            z4_r = img[x,y+1][0] / 1.0
            z4_g = img[x,y+1][1] / 1.0
            z4_b = img[x,y+1][2] / 1.0

            z5_r = img[x+1,y+1][0] / 1.0
            z5_g = img[x+1,y+1][1] / 1.0
            z5_b = img[x+1,y+1][2] / 1.0

            z6_r = img[x+2,y+1][0] / 1.0
            z6_g = img[x+2,y+1][1] / 1.0
            z6_b = img[x+2,y+1][2] / 1.0

            z8_r = img[x+1,y+2][0] / 1.0
            z8_g = img[x+1,y+2][1] / 1.0
            z8_b = img[x+1,y+2][2] / 1.0

            length_r = np.abs((z2_r + z4_r - 4*z5_r + z6_r + z8_r))
            if length_r > max_r:
                max_r = length_r
            vis[x+1, y+1][0] = length_r

            length_g = np.abs((z2_g + z4_g - 4*z5_g + z6_g + z8_g))
            if length_g > max_g:
                max_g = length_g
            vis[x+1, y+1][1] = length_g

            length_b = np.abs((z2_b + z4_b - 4*z5_b + z6_b + z8_b))
            if length_b > max_b:
                max_b = length_b
            vis[x+1, y+1][2] = length_b


    for x in range(1, height-rank):
        for y in range(1, width-rank):
            vis[x+1, y+1][0] = vis[x+1, y+1][0] / max_r * 255
            vis[x+1, y+1][1] = vis[x+1, y+1][1] / max_g * 255
            vis[x+1, y+1][2] = vis[x+1, y+1][2] / max_b * 255
    return vis

def sharpness(img, rank = 3):
    height, width = img.shape[:2]
    vis = np.zeros((height, width, 3), np.uint8)
    vis[:height, :width,:3] = img[:height, :width, :1]
    cv2.imwrite("7-sharpness-source.jpg", vis)
    img = cv2.imread('7-sharpness-source.jpg')
    height, width = img.shape[:2]
    vis = np.zeros((height, width, 3), np.uint8)
    vis[:height, :width,:3] = img[:height, :width, :3]

    k = 5
    max_r = 0
    max_g = 0
    max_b = 0
    for x in range(1, height-rank):
        print(height - x - rank)
        for y in range(1, width-rank):
            z1 = img[x,y][0] / 1.0
            z2 = img[x+1,y][0] / 1.0
            z3 = img[x+2,y][0] / 1.0
            z4 = img[x,y+1][0] / 1.0
            z5 = img[x+1,y+1][0] / 1.0
            z6 = img[x+2,y+1][0] / 1.0
            z7 = img[x,y+2][0] / 1.0
            z8 = img[x+1,y+2][0] / 1.0
            z9 = img[x+2,y+2][0] / 1.0

            length = +(-z1 - z2 - z3 - z4 + k*z5 - z6 - z7 - z8 - z9) / np.sqrt(k)
            #length = -(z2 + z4 - k*z5 + z6 + z8)
            vis[x+1, y+1] = [length, length, length]

    # for x in range(1, height-rank):
    #     for y in range(1, width-rank):
    #         vis[x, y][0] = vis[x, y][0] / max_r * 255
    #         vis[x, y][1] = vis[x, y][1] / max_g * 255
    #         vis[x, y][2] = vis[x, y][2] / max_b * 255
    cv2.imwrite("7-sharpness-"+str(k)+".jpg", vis)
    return vis

img = cv2.imread('1-sourse.jpg')
# vis = average(img, 5)
# cv2.imwrite("2-average.jpg",vis)
#
# vis = med(img, 5)
# cv2.imwrite("3-med.jpg",vis)
#
# vis = sobel(img)
# cv2.imwrite("4-sobel.jpg",vis)
#
# vis = prewitt(img)
# cv2.imwrite("5-prewitt.jpg",vis)

# vis = laplas(img)
# cv2.imwrite("6-laplas.jpg",vis)

img = cv2.imread('2-average.jpg')
vis = sharpness(img)
cv2.imwrite("7-sharpness.jpg", vis)


#vis = np.zeros((height, width, 3), np.uint8)



#cv2.imshow("test", vis)

cv2.waitKey()
