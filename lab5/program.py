import numpy as np
from matplotlib import pyplot as plt
import cv2
import canny as cn


img = cv2.imread('source.jpg', 0)

mask = [
    2, 4, 5, 4, 2,
    4, 9, 12, 9, 4,
    5, 12, 15, 12, 5,
    4, 9, 12, 9, 4,
    2, 4, 5, 4, 2
]

height, width = img.shape[:2]



edges = cn.canny(img, 0.2)

plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()
vis = np.zeros((height, width), np.uint8)
# vis[:height, :width] = img[:height, :width]
