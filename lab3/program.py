import numpy as np
from matplotlib import pyplot as plt
import cv2



img = cv2.imread('1-source.jpg', 0) # читаем файл

img_float32 = np.float32(img) # переводим в формат float32

dft = cv2.dft(np.float32(img), flags=cv2.DFT_COMPLEX_OUTPUT) # применяем функцию opencv. DFT - discrete fourier tranform
dft_shift = np.fft.fftshift(dft) # Смотри ниже что делает эта функция

##################### FFTSHIFT - сдивигает нулевые частоты в центр спектра
##################### >>> freqs
##################### array([ 0.,  1.,  2.,  3.,  4., -5., -4., -3., -2., -1.])
##################### >>> np.fft.fftshift(freqs)
##################### array([-5., -4., -3., -2., -1.,  0.,  1.,  2.,  3.,  4.])

## cv2.magnitude находим расстояния между векторами равносильно c = sqrt(a^2 + b^2)
## dft_shift - трехмерный массив, берем первый и второй элемент и находим расстояние, логарифмируем и на 20
## по сути объяснять не надо, формула такая
## Почему двоеточия в массиве? это означает, что по сути мы в цикле выполняем это действие, в питоне можно сделать сразу
##
magnitude_spectrum = 20 * np.log(cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))


rows, cols = img.shape # размеры изображения
crow, ccol = int(rows / 2), int(cols / 2)  # center

## Для нискочастотного делаем маску из нулей в центре, по сути можно сделать ее любыми способами,
## здесь является круглой благодаря питонским прибамбасам
# ## Low pass filter mask - раскомментировать ниже, чтобы работал низкочастотный, а высокочастотный закомментировать
# mask = np.zeros((rows, cols, 2), np.uint8)
# r = 80
# center = [crow, ccol]
# x, y = np.ogrid[:rows, :cols]
# mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
# mask[mask_area] = 1
## END OF LOW PASS FILTER MASK


# High pass filter mask - та же логика, но наоборот
mask = np.ones((rows, cols, 2), np.uint8)
r = 40
center = [crow, ccol]
x, y = np.ogrid[:rows, :cols]
mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
mask[mask_area] = 0
## END OF HIGH PASS FILTER MASK


# apply mask and inverse DFT
fshift = dft_shift * mask
fshift_mask_mag = 20 * np.log(cv2.magnitude(fshift[:, :, 0], fshift[:, :, 1]))


f_ishift = np.fft.ifftshift(fshift) ## Смотри ниже ,что делает эта функция
#################### >>> freqs
#################### array([[ 0.,  1.,  2.],
####################        [ 3.,  4., -4.],
####################        [-3., -2., -1.]])
#################### >>> np.fft.ifftshift(np.fft.fftshift(freqs))
#################### array([[ 0.,  1.,  2.],
####################        [ 3.,  4., -4.],
####################        [-3., -2., -1.]])
#################### ОДинаковый результат отзначает, что ifftshift обратна fftshift


## Функции opencv документации в инете есть
img_back = cv2.idft(f_ishift)
img_back = cv2.magnitude(img_back[:, :, 0], img_back[:, :, 1])

## Тут просто выводы, в том числе img_back - результат
plt.subplot(2, 2, 1), plt.imshow(img, cmap='gray')
plt.title('Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 2, 2), plt.imshow(magnitude_spectrum, cmap='gray')
plt.title('FFT'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 2, 3), plt.imshow(fshift_mask_mag, cmap='gray')
plt.title('FFT + Mask'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 2, 4), plt.imshow(img_back, cmap='gray')
plt.title('After FFT Inverse'), plt.xticks([]), plt.yticks([])
plt.show()

cv2.waitKey()
