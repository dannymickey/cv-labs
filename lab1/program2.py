import numpy as np
import cv2
cropping = False



def mouse_crop(event, x, y, flags, param):
    # grab references to the global variables
    global x_start, y_start, x_end, y_end, cropping
    if event == cv2.EVENT_LBUTTONDOWN:
        x_start, y_start, x_end, y_end = x, y, x, y
        cropping = True

    elif event == cv2.EVENT_MOUSEMOVE:
        if cropping == True:
            x_end, y_end = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        x_end, y_end = x, y
        cropping = False

        refPoint = [(x_start, y_start), (x_end, y_end)]

        if len(refPoint) == 2:
            roi = imS[refPoint[0][1]:refPoint[1][1], refPoint[0][0]:refPoint[1][0]]
            cv2.imshow("Cropped", roi)
            cv2.imwrite("crop1.jpg",roi)


img = cv2.imread('animal.jpg', cv2.IMREAD_UNCHANGED)
cv2.namedWindow('image', cv2.WINDOW_AUTOSIZE)
imS = cv2.resize(img, (800, 600))
cv2.imshow('image',imS)
cv2.setMouseCallback('image', mouse_crop)
while(1):
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break


cv2.destroyAllWindows()
