import numpy as np
import cv2

# Load an color image in grayscale
img = cv2.imread('animal.jpg', cv2.IMREAD_UNCHANGED)

cv2.namedWindow('First program', cv2.WINDOW_NORMAL)
cv2.imshow('First program',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
