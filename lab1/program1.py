import cv2
import numpy as np

drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1

def drawTriangle(event, x, y, flags, param):
    global ix, iy, drawing, mode

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        pts = np.array([[ix,y],[(ix+x)/2, iy],[x, y]], np.int32)
        cv2.polylines(img,[pts],True,(0, 255, 255))


img = np.zeros((800, 800, 3), np.uint8)
cv2.namedWindow('image')
cv2.setMouseCallback('image', drawTriangle)

while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    elif k == ord('s'):
        cv2.imwrite('picture.png',img)

cv2.destroyAllWindows()
