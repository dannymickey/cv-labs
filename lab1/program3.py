import numpy as np
import cv2

img1 = cv2.imread('crop.jpg')
img2 = cv2.imread('crop1.jpg')

h1, w1 = img1.shape[:2]
h2, w2 = img2.shape[:2]

#create empty matrix
vis = np.zeros((max(h1, h2), w1+w2,3), np.uint8)

#combine 2 images
vis[:h1, :w1,:3] = img1
vis[:h2, w1:w1+w2,:3] = img2

cv2.imshow("test", vis)
cv2.imwrite("result.jpg",vis)
cv2.waitKey()
